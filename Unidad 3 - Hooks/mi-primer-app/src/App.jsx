import './App.css';
import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductContainer from './containers/ProductContainer';

function App() {
  return (
    <div className='App'>
      <ProductContainer/>
    </div>
  );
}

export default App;
