import React from 'react'
import { useState } from 'react';

const CardComponent = ({title, img, agregarAlCarrito}) => {

    console.log("Se esta por renderizar")

    return (
        <div className="card col-4" style={{width: "18rem;"}}>
        <img classNameName="card-img-top" src={img} alt="Card image cap"/>
        <div className="card-body">
            <h5 className="card-title">{title}</h5>
            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <button className="btn btn-primary" onClick={(event) => {agregarAlCarrito(event, 'perrito')}}>Agregar al carrito</button>
        </div>
        </div>
    )
}

export default CardComponent;