import React from 'react'
import { useState } from 'react';
import CardComponent from '../component/CardComponent';

const ProductContainer = () => {

    const [carrito, setCarrito] = useState([])

    const AgregarAlCarrito = (event, name) => {
        carrito.push(name);
        setCarrito([...carrito]);
    }

    return (
        <div className='container bg-warning'>
            <div className='bg-info'>
                {carrito.length}
            </div>

            <div className='row'>
                <CardComponent title={'Hola mundo'} img={'https://www.marthadebayle.com/wp-content/uploads/2021/07/Dia-mundial-del-perro.jpg'} agregarAlCarrito={AgregarAlCarrito}/>
                {/* {<CardComponent/>
                <CardComponent/>
                <CardComponent/>
                <CardComponent/>} */}
            </div>
        </div>
    )
}

export default ProductContainer;