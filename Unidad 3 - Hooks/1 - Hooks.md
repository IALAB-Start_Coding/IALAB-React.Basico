# **Hooks**

## **Hooks**

[Ver: Eventos, Hooks y ciclos de vida.pdf]

- **Interés:**
  - https://es.reactjs.org/docs/hooks-intro.html

---

## **¿ Y que son los hooks ?**

Los React hooks permiten la construccion de componentes mucho mas limpios y reutilizables, sin la necesidad de escribir componentes dentro de clases.

### **Hooks vs Classes**

Los hooks son una implementacion que vienen a reemplazar la utilizacion de las funciones dentro de una clase como puede ser ComponentDidMount, para que los componentes basados en funciones, puedan respetar el mismo comportamiento. Es una de las mejores mejoras que implementaron en React.

![Hooks vs Classes](img/Hooks_vs_classes.png "HvsC")

### **Hooks Personalizados**

![Hooks Personalizados](img/Hooks_Personalizados.png "HP")
