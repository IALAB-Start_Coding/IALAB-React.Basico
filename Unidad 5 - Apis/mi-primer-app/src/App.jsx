import './App.css';
import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductContainer from './containers/ProductContainer';
import EcommerceProvider from './context/EcommerceContext';

function App() {
  return (
    <div className='App'>
      <EcommerceProvider>
        <ProductContainer/>
      </EcommerceProvider>
    </div>
  );
}

export default App;
