# **Curso IALAB - Start Coding - React.Basico**

Curso de Inteligencia Artificial - Start Coding - React.Basico

## **Unidad 1: Introducción a React.js**

1. Introducción
2. Instalación de Node.JS
3. Creación de nuestra primer React App

## **Unidad 2: Componentes y Eventos**

1. Componentes & Componentes de presentacion
2. Componentes Contenedores
3. Eventos

## **Unidad 3: Hooks**

1. Hooks
2. ¿Y que son los hooks?

## **Unidad 4: Miscelaneos**

1. Ciclos de vida
2. Tecnicas de rendering
3. Context

## **Unidad 5: Apis**

1. Consumiendo APIS

## **Unidad 6: Navegación**

1. Routing
2. Navegación entre componentes
3. Rutas dinámicas
4. Rutas dinámicas y contenido

## **Unidad 7: El store - Redux**

1. Filtrando estados
2. Introducción a Redux - Configuracion de Redux
3. Consumiento store - useSelector
4. Actualizando nuestro store - useDispatch
5. Github Repository

## **Unidad 8: Forms**

1. Introducción a Forms
2. Validaciones Básicas
3. Validaciones Avanzadas
4. Validaciones on Submit onBlur
5. Use Formik
6. RegEx
