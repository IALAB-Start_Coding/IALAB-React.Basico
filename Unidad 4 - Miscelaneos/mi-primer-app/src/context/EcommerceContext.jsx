import React from 'react'

const { createContext, useState, useEffect } = require("react");

export const EcommerceContext = createContext();

const EcommerceProvider = ({children}) => {

    const [products, setProducts] = useState([])

    useEffect(() => {
        
        async function fetchData() {
            const data = await fetch('https://api.mercadolibre.com/sites/MLA/search?q=mac');
            const response = await data.json();
            setProducts(response.results);
        }

        fetchData();
    }, [])

    return <EcommerceContext.Provider value={{products}}>
        {children}
    </EcommerceContext.Provider>
}

export default EcommerceProvider;
