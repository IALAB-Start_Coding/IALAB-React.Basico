import React, { useState, useEffect, useContext } from 'react'
import CardComponent from '../component/CardComponent';
import InfoBarComponent from '../component/InfoBarComponent';
import { EcommerceContext } from '../context/EcommerceContext';

const ProductContainer = () => {

    const [carrito, setCarrito] = useState([])

    const {products} = useContext(EcommerceContext)

    useEffect(async() => {
        // Para cuando se esta para montar un componente-
        // Llamada a una API
        
        // const data = [
        //     {id:1, name:"shoes", img:"https://www.marthadebayle.com/wp-content/uploads/2021/07/Dia-mundial-del-perro.jpg"},
        //     {id:2, name:"shirt", img:"https://www.marthadebayle.com/wp-content/uploads/2021/07/Dia-mundial-del-perro.jpg"},
        //     {id:3, name:"book", img:"https://www.marthadebayle.com/wp-content/uploads/2021/07/Dia-mundial-del-perro.jpg"},
        // ];
        // setProducts(data)


        // const data = await fetch('https://api.mercadolibre.com/sites/MLA/search?q=mac');
        // const response = await data.json();
        // setProducts(response.results);

        return()=>{
            console.log("Se esta por morir ese componente");
        }
    }, []);

    const AgregarAlCarrito = (event, name) => {
        carrito.push(name);
        setCarrito([...carrito]);
    }

    console.log(carrito)

    return (
        <div className='container bg-warning'>
            <InfoBarComponent carrito={carrito}/>
            <div className='row px-2 py-2'> 
                {products.map(element => {
                    return (    
                        <CardComponent title={element.name} img={element.img} agregarAlCarrito={AgregarAlCarrito}/>
                    )
                })}

            </div>
        </div>
    )
}

export default ProductContainer;