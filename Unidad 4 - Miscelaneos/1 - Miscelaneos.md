# **Miscelaneos**

## **Ciclos de vida**

[Ver : Eventos, Hooks y ciclos de vida.pdf]

- **Interés**:
  - https://es.reactjs.org/docs/state-and-lifecycle.html

---

## **Tecnicas de rendering**

[Ver : Eventos, Hooks y ciclos de vida.pdf]

---

## **Context**

[Ver : Context.pdf]

- **Interés**:
  - https://es.reactjs.org/docs/context.html
