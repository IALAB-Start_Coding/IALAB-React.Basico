import React from 'react'

const { createContext, useState } = require("react");

export const EcommerceContext = createContext();

const EcommerceProvider = ({children}) => {

    const [products, setProducts] = useState([])
    const [carrito, setCarrito] = useState([])
    
    async function fetchData(searchQuery) {
        const data = await fetch(`https://api.mercadolibre.com/sites/MLA/search?q=${searchQuery}`);
        const response = await data.json();
        setProducts(response.results);
    }
    
    return <EcommerceContext.Provider value={{ products, carrito, setCarrito, fetchData }}>
        {children}
    </EcommerceContext.Provider>
}

export default EcommerceProvider;
