import './App.css';
import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductContainer from './containers/ProductContainer';
import EcommerceProvider from './context/EcommerceContext';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import HomeContainer from './containers/HomeContainer';

function App() {
  return (
    <div className='App'>

        <BrowserRouter>
          <EcommerceProvider>
                <React.Fragment>
            <Routes>
              <Route exact path="/" element={ <HomeContainer/> } />
              <Route exact path="/productos" element={ <ProductContainer/> } />
              <Route path="/productos/:busqueda" element={ <ProductContainer/> } />
            </Routes>
                </React.Fragment>
          </EcommerceProvider>
        </BrowserRouter>

    </div>
  );
}

export default App;
