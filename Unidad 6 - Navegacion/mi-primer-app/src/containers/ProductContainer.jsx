import React, { useEffect, useContext } from 'react'
import { useParams } from 'react-router-dom';
import CardComponent from '../component/CardComponent';
import InfoBarComponent from '../component/InfoBarComponent';
import { EcommerceContext } from '../context/EcommerceContext';

const ProductContainer = () => {

    const {products, carrito, setCarrito, fetchData} = useContext(EcommerceContext)
    const { busqueda } = useParams();

    useEffect(async() => {
        // Para cuando se esta para montar un componente-
        // Llamada a una API
        
        // Hardcoding
        // const data = [
        //     {id:1, name:"shoes", img:"https://www.marthadebayle.com/wp-content/uploads/2021/07/Dia-mundial-del-perro.jpg"},
        //     {id:2, name:"shirt", img:"https://www.marthadebayle.com/wp-content/uploads/2021/07/Dia-mundial-del-perro.jpg"},
        //     {id:3, name:"book", img:"https://www.marthadebayle.com/wp-content/uploads/2021/07/Dia-mundial-del-perro.jpg"},
        // ];
        // setProducts(data)

        // Consumo de API
        // const data = await fetch('https://api.mercadolibre.com/sites/MLA/search?q=mac');
        // const response = await data.json();
        // setProducts(response.results);

        fetchData(busqueda)
        return()=>{
            console.log("Se esta por morir ese componente");
        }
    }, [busqueda]);

    const AgregarAlCarrito = (event, product) => {
        carrito.push(product);
        setCarrito([...carrito]);
        console.log(carrito);
    }

    return (
        <div className='container bg-warning'>
            <InfoBarComponent carrito={carrito}/>
            <div className='row px-2 py-2'> 
                {products.map((element, index) => {
                    return (
                        <span className=' col-4' key={index}>
                            <CardComponent product={element} agregarAlCarrito={AgregarAlCarrito}/>
                        </span>
                    )
                })}

            </div>
        </div>
    )
}

export default ProductContainer;