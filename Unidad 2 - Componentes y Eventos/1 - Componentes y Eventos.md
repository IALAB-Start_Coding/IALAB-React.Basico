# **Componentes y Eventos**

## **Componentes & Componentes de presentacion**

[Ver : Componentes.pdf]

---

## **Componentes Contenedores**

Tienen como proposito **encapsular a otros** componentes y **proporcionarles las propiedades** que necesitan. Ademas se encargan de **modificar el estado** de las aplicaciones para que el usuario vea el cambio en los datos (por eso son tambien llamados **state** components).

**Codigo**

- _En components_ : componente que se encarga de hacer logica.
- _En containers_ : componente de vista/presentacion.

app.js es de logica

- **Material extra**
  - https;//es.reactjs.org/docs/components-and-props.html
  - https;//es.reactjs.org/docs/introducing-jsx.html

---

## **Eventos**

[Ver : Eventos, Hooks y ciclos de vida.pdf]
